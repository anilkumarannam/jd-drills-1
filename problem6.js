function problem6(inventory) {

    if (inventory !== undefined || inventory !== null) {
        let BMWAudiList = [];
        let inventorySize = inventory.length;
        for (let index = 0; index < inventorySize; index++) {
            let car = inventory[index];
            if (car.car_make === "BMW" || car.car_make === "Audi") {
                BMWAudiList.push(car)
            }
        }
        console.log(JSON.stringify(BMWAudiList));
    }
}

module.exports = problem6;