function problem5(inventory) {

    if (inventory !== undefined || inventory !== null) {
        let carYearList = [];
        let inventorySize = inventory.length;
        for (let index = 0; index < inventorySize; index++) {
            let car = inventory[index];
            if (car.car_year < 2000) {
                carYearList.push(car.car_year)
            }
        }
        console.log(carYearList.length);
    }
}

module.exports = problem5;