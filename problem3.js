function problem3(inventory) {

    if (inventory !== undefined || inventory !== null) {
        let carModelList = [];
        let inventorySize = inventory.length;
        for (let index = 0; index < inventorySize; index++) {
            let car = inventory[index];
            carModelList.push(car.car_model)
        }
        console.log(carModelList.sort());
    }
}

module.exports = problem3;