function problem2(inventory) {
    let car = inventory[inventory.length - 1];
    let outPutString = "Last car is a " + car.car_make + " car " + car.car_year + " " + car.car_make + " " + car.car_model;
    console.log(outPutString);
}

module.exports = problem2;